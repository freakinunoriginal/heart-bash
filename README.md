# heart-bash

A version of my bashrc with a unicode heart symbol.

## Example

![Example](https://gitlab.com/freakinunoriginal/heart-bash/raw/master/hint.png)