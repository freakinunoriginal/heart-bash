# Looks something like:
# ░▒ user ▒ host ░ where ▒░
# ♥
# With transitional colors (green-blue-yellow)
# Scott Bay, 2019, no rights reserved it's like the most basic bashrc
if [[ $- == *i* ]]; then # apparently needs this to keep Ubuntu from freaking out on login
# define my colors
txtWhite=$(tput setaf 231);
bgGreen=$(tput setab 43);
txtGreen=$(tput setaf 43);
bgBlue=$(tput setab 69);
bgYellow=$(tput setab 221);
txtYellow=$(tput setaf 221);
txtPink=$(tput setaf 219);
reset=$(tput sgr0);
# PS1 starts here
PS1="\[${txtGreen}\]░▒"; # fade in
PS1+="\[${txtWhite}\]\[${bgGreen}\]";
PS1+=" \u "; # user
PS1+="\[${txtGreen}\]\[${bgBlue}\]▒";
PS1+="\[${txtYellow}\]";
PS1+=" \h ░"; # host/machine name; light shaded block because of color contrast
PS1+="\[${txtWhite}\]\[${bgYellow}\]";
PS1+=" \w "; # where/current dir (full path)
PS1+="\[${reset}\]\[${txtYellow}\]▒░"; # fade out
PS1+='\[${txtPink}\]\n♥ \[${reset}\]'; # cursor starts here
export PS1;
fi
